package front;

import PGP.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import org.bouncycastle.openpgp.*;

import java.io.*;

public class EncryptionController {

    private FileChooser fileChooser = new FileChooser();
    private File selectedFile;
    private static String fileToEncrypt;
    private static String fileToDecrypt;
    private static String publicKeyFile;
    private static String privateKeyFile;
    private static String passphrase;
    private static String fileToSign;
    private static String fileDecrypted = System.getProperty("user.home") + "/Desktop/decrypted.";
    private static String encryptedFilePath = System.getProperty("user.home") + "/Desktop/x.pgp";

    private static String signPublicKey;
    private static String signPrivateKey;

    @FXML
    TextField encryptionFilePath;
    @FXML
    TextField publicKeyPath;
    @FXML
    TextField decryptionFilePath;
    @FXML
    TextField secretKeyPath;
    @FXML
    TextField signFilePath;
    @FXML
    TextField signSecretKeyPath;
    @FXML
    TextField signPublicKeyPath;
    @FXML
    PasswordField signPrivateKeyPasswordField;
    @FXML
    Button encryptionFileChooserButton;
    @FXML
    Button decryptionFileChooserButton;
    @FXML
    Button publicKeyFileChooserButton;
    @FXML
    Button secretKeyChooseFileButton;
    @FXML
    PasswordField privateKeyPasswordField;
    @FXML
    Button encryptButton;
    @FXML
    Button decryptButton;
    @FXML
    RadioButton RSAMethodRadioButton;
    @FXML
    RadioButton PGPMethodRadioButton;
    @FXML
    RadioButton RSADecryptMethodRadioButton;
    @FXML
    RadioButton PGPDecryptMethodRadioButton;
    @FXML
    TextField fileExtensionTextField;
    @FXML
    Button signFileChooserButton;
    @FXML
    Button signSecretKeyFileChooseButton;
    @FXML
    Button signPublicKeyFileChooseButton;


    public void setPathFromFile(ActionEvent e) {
        Button selectedButton = (Button) e.getSource();

        selectedFile = fileChooser.showOpenDialog(null);
        if (selectedButton == encryptionFileChooserButton) {
            encryptionFilePath.setText(selectedFile.getAbsolutePath());
            fileToEncrypt = encryptionFilePath.getText();
        } else if (selectedButton == decryptionFileChooserButton) {
            decryptionFilePath.setText(selectedFile.getAbsolutePath());
            fileToDecrypt = decryptionFilePath.getText();
        } else if (selectedButton == publicKeyFileChooserButton) {
            publicKeyPath.setText((selectedFile.getAbsolutePath()));
            publicKeyFile = publicKeyPath.getText();
        } else if (selectedButton == secretKeyChooseFileButton) {
            secretKeyPath.setText((selectedFile.getAbsolutePath()));
            privateKeyFile = secretKeyPath.getText();
        } else if (selectedButton == signFileChooserButton) {
            signFilePath.setText((selectedFile.getAbsolutePath()));
            fileToSign = signFilePath.getText();
        } else if (selectedButton == signSecretKeyFileChooseButton) {
            signSecretKeyPath.setText((selectedFile.getAbsolutePath()));
            signPrivateKey = signSecretKeyPath.getText();
        } else if (selectedButton == signPublicKeyFileChooseButton) {
            signPublicKeyPath.setText((selectedFile.getAbsolutePath()));
            signPublicKey = signPublicKeyPath.getText();
        }
    }

    public static void encryptPGP() {
        try {
            FileInputStream keyIn = new FileInputStream(publicKeyFile);
            FileOutputStream out = new FileOutputStream(encryptedFilePath);
            PGPPublicKey publicKey = PGPEncryption.readPublicKey(keyIn);
            PGPEncryption.encryptFile(out, fileToEncrypt, publicKey, true, true);

            out.close();
            keyIn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void decryptPGP() {
        fileDecrypted = System.getProperty("user.home") + "/Desktop/decrypted.";
        fileDecrypted += fileExtensionTextField.getText();
        passphrase = privateKeyPasswordField.getText();
        try {
            FileInputStream in = new FileInputStream(fileToDecrypt);
            FileOutputStream out = new FileOutputStream(fileDecrypted);
            FileInputStream keyIn = new FileInputStream(privateKeyFile);
            PGPDecryption.decryptFile(in, out, keyIn, passphrase.toCharArray());
            in.close();
            out.close();
            keyIn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void signPGP() {
        passphrase = signPrivateKeyPasswordField.getText();
        try {
            signFileDetached();
            verifyFileDetached();

            /*
                PGPSignFile.createSignature(System.getProperty("user.home") + "/Desktop/signed.sig", new FileInputStream(signPrivateKey), new FileInputStream(signPublicKey),out, passphrase.toCharArray(), true);
                PGPSignPublicKey.signEncryptFile(new FileOutputStream(System.getProperty("user.home") + "/Desktop/signed.pgp"), fileToSign, publicKey, secretKey, passphrase, true, true);
            */
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void signFileDetached() throws IOException, PGPException {
        PGPPublicKey publicKey = PGPEncryption.readPublicKey(new FileInputStream(signPublicKey));
        File fileToSign = new File(signFilePath.getText());
        File signatureFile = new File(signFilePath.getText() + ".sig");

        try {
            PGPSignFile.signFileDetached(fileToSign, publicKey, new FileInputStream(signPrivateKey), signatureFile, signPrivateKeyPasswordField.getText().toCharArray(), false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void verifyFileDetached() throws IOException, PGPException {
        PGPPublicKey publicKey = PGPEncryption.readPublicKey(new FileInputStream(signPublicKey));
        File fileToVerify = new File(signFilePath.getText());
        File signatureFile = new File(signFilePath.getText() + ".sig");
        try {
            boolean verified = PGPSignFile.verifyFileDetached(fileToVerify, signatureFile, publicKey);
            System.out.println("Verified: " + verified);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void signFile() {
        signPGP();
        showSuccesfullEncryptionDecryption("Signed");
    }

    public void encryptFile() {
        encryptPGP();
        showSuccesfullEncryptionDecryption("Encrypted");
    }

    public void decryptFile() {
        decryptPGP();
        showSuccesfullEncryptionDecryption("Decrypted");

    }

    private static void showSuccesfullEncryptionDecryption(String method) {
        Alert succesfullAlert = new Alert(Alert.AlertType.INFORMATION);
        succesfullAlert.setContentText(method + " finished without errors");
        succesfullAlert.showAndWait();
    }

    public String getFileEncrypted() {
        return encryptedFilePath;
    }


}
