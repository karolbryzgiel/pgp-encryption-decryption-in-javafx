package PGP;

import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentVerifierBuilderProvider;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.Iterator;

import static PGP.PGPDecryption.readSecretKey;

public class PGPSignFile {

    public static void signFileDetached(File fileToSign, PGPPublicKey pgpPublicKey, InputStream secretKeyFile, File outputFile, char[] passphrase, boolean asciiArmor) throws IOException, PGPException, SignatureException, NoSuchProviderException, NoSuchAlgorithmException {


        OutputStream outputStream = null;
        if (asciiArmor) {
            outputStream = new ArmoredOutputStream(new BufferedOutputStream(new FileOutputStream(outputFile)));
        }
        else {
            outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));
        }

        PGPSecretKey pgpSecretKey = readSecretKey(secretKeyFile);
        PGPPrivateKey pgpPrivateKey = pgpSecretKey
                .extractPrivateKey(passphrase, new BouncyCastleProvider());
        PGPSignatureGenerator signatureGenerator = new PGPSignatureGenerator(
                pgpPublicKey.getAlgorithm(),
                PGPUtil.SHA512,
                new BouncyCastleProvider());
        signatureGenerator.initSign(PGPSignature.BINARY_DOCUMENT, pgpPrivateKey);

        BCPGOutputStream bOut = new BCPGOutputStream(outputStream);
        InputStream fIn = new BufferedInputStream(new FileInputStream(fileToSign));

        int ch;
        while ((ch = fIn.read()) >= 0) {
            signatureGenerator.update((byte)ch);
        }

        fIn.close();

        signatureGenerator.generate().encode(bOut);

        outputStream.close();
    }

    public static boolean verifyFileDetached(File fileToVerify, File signatureFile, PGPPublicKey publicKeyFile) throws FileNotFoundException, IOException, PGPException, SignatureException {
        InputStream sigInputStream = PGPUtil.getDecoderStream(new BufferedInputStream(new FileInputStream(signatureFile)));

        PGPObjectFactory pgpObjFactory = new PGPObjectFactory(sigInputStream);
        PGPSignatureList pgpSigList = null;

        Object obj = pgpObjFactory.nextObject();
        if (obj instanceof PGPCompressedData) {
            PGPCompressedData c1 = (PGPCompressedData)obj;
            pgpObjFactory = new PGPObjectFactory(c1.getDataStream());
            pgpSigList = (PGPSignatureList)pgpObjFactory.nextObject();
        }
        else {
            pgpSigList = (PGPSignatureList)obj;
        }


        InputStream  fileInputStream = new BufferedInputStream(new FileInputStream(fileToVerify));
        PGPSignature sig = pgpSigList.get(0);
        PGPPublicKey pubKey = publicKeyFile;
        sig.initVerify(pubKey, new BouncyCastleProvider());

        int ch;
        while ((ch = fileInputStream.read()) >= 0) {
            sig.update((byte)ch);
        }

        fileInputStream.close();
        sigInputStream.close();

        if (sig.verify()) {
            return true;
        }
        else {
            return false;
        }
    }
/*
    public static void createSignature(
            String fileName,
            InputStream keyIn,
            InputStream pubKeyIn,
            OutputStream out,
            char[] pass,
            boolean armor)
            throws IOException, NoSuchAlgorithmException, NoSuchProviderException, PGPException, SignatureException {
        if (armor) {
            out = new ArmoredOutputStream(out);
        }

        PGPSecretKey pgpSec = readSecretKey(keyIn);
        PGPPublicKey pgpPublicKey = PGPEncryption.readPublicKey(pubKeyIn);
        PGPPrivateKey pgpPrivKey = pgpSec.extractPrivateKey(pass, new BouncyCastleProvider());
        PGPSignatureGenerator sGen = new PGPSignatureGenerator(pgpPublicKey.getAlgorithm(), PGPUtil.SHA1, new BouncyCastleProvider());

        sGen.initSign(PGPSignature.BINARY_DOCUMENT, pgpPrivKey);

        BCPGOutputStream bOut = new BCPGOutputStream(out);

        FileInputStream fIn = new FileInputStream(fileName);
        int ch = 0;

        while ((ch = fIn.read()) >= 0) {
            sGen.update((byte) ch);
        }

        sGen.generate().encode(bOut);

        out.close();
    }
    */

}
