import PGP.PGPDecryption;
import PGP.PGPEncryption;
import front.EncryptionController;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import org.junit.Assert;
import org.junit.Test;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class EncryptionTest extends Component {

    static String publicKeyFile = null;
    static String fileToEncrypt = null;
    static String encryptedFilePath = null;
    static JFileChooser fileChooser;

    @Test
    public void checkThatEncryptionFileIsExists() throws Exception {
        EncryptionController encryptionController = new EncryptionController();
        getFilePaths();
        encryptFile(encryptedFilePath, fileToEncrypt, publicKeyFile);

        File encryptedFile = new File(encryptionController.getFileEncrypted());
        Assert.assertTrue(encryptedFile.exists());
        Assert.assertTrue(encryptedFile.length() != 0);
    }

    public static void getFilePaths() {
        publicKeyFile = openDialogToChooseFiles("Choose public key file", "all");
        fileToEncrypt = openDialogToChooseFiles("Choose file to encrypt", "all");
        encryptedFilePath = openDialogToChooseFiles("Choose place and filename for encrypted file", "pgp");
    }

    public static String openDialogToChooseFiles(String name, String fileExtension) {
        FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("PGP File", fileExtension);
        JFileChooser jFileChooser = new JFileChooser();
        File workingDirectory = new File(System.getProperty("user.home") + "/Desktop");
        jFileChooser.setCurrentDirectory(workingDirectory);
        jFileChooser.setDialogTitle(name);
        if( fileExtension != "all") {
            jFileChooser.setFileFilter(fileNameExtensionFilter);
        }
        jFileChooser.showOpenDialog(fileChooser);
        File fileChoosen = jFileChooser.getSelectedFile();
        return fileChoosen.getAbsolutePath();
    }


    private static void encryptFile(String encryptedFilePath, String fileToEncrypt, String publicKeyFile) throws Exception {
        FileInputStream keyIn = new FileInputStream(publicKeyFile);
        FileOutputStream out = new FileOutputStream(encryptedFilePath);
        PGPEncryption.encryptFile(out, fileToEncrypt, PGPEncryption.readPublicKey(keyIn), true, true);
        out.close();
        keyIn.close();
    }
}
