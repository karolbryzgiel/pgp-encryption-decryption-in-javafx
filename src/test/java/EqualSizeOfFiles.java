import PGP.RSAEncryptionFile;
import front.EncryptionController;
import org.junit.Assert;
import org.junit.Test;

import javax.swing.*;
import java.io.File;

public class EqualSizeOfFiles {

    private static String afterDecrypted;
    private static String beforeEncrypted;
    @Test
    public void checkThatSizeOfFilesIsSame() {
        getFilePaths();
        File encryptedFile = new File(beforeEncrypted);
        File decryptedFile = new File(afterDecrypted);
        Assert.assertTrue(encryptedFile.length() == decryptedFile.length());
    }
    private static void getFilePaths() {
        beforeEncrypted = EncryptionTest.openDialogToChooseFiles("Choose file before encrypted", "all");
        afterDecrypted = EncryptionTest.openDialogToChooseFiles("Choose file after decrypted", "all");
    }
}
