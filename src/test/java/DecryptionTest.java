import PGP.PGPDecryption;
import front.EncryptionController;
import javafx.event.ActionEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.junit.Assert;
import org.junit.Test;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class DecryptionTest {
    private static String privateKeyFile;
    private static String fileToDecrypt;
    private static String fileDecrypted;
    private static String passphrase;

    @Test
    public void checkThatDecryptionFileIsExists() throws Exception {
        getFilePaths();
        decryptFile(fileToDecrypt, fileDecrypted, privateKeyFile, passphrase);

        File decryptionFile = new File(fileDecrypted);
        Assert.assertTrue(decryptionFile.exists());
        Assert.assertTrue(decryptionFile.length() != 0);
    }

    public static void getFilePaths() {
        privateKeyFile = EncryptionTest.openDialogToChooseFiles("Choose private key file", "all");
        fileToDecrypt = EncryptionTest.openDialogToChooseFiles("Choose file to decrypt", "pgp");
        fileDecrypted = EncryptionTest.openDialogToChooseFiles("Choose filename and extension type", "all");
        JPasswordField pf = new JPasswordField();
        int okCxl = JOptionPane.showConfirmDialog(null, pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        passphrase = new String(pf.getPassword());
    }

    private static void decryptFile(String fileToDecrypt, String fileDecrypted, String privateKeyFile, String passphrase) throws Exception {
        FileInputStream in = new FileInputStream(fileToDecrypt);
        FileOutputStream out = new FileOutputStream(fileDecrypted);
        FileInputStream keyIn = new FileInputStream(privateKeyFile);
        PGPDecryption.decryptFile(in, out, keyIn, passphrase.toCharArray());
        in.close();
        out.close();
        keyIn.close();
    }
}
